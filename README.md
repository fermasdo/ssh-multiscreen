# SSH-multiscreen

The aim of this project is to practice remote administration using SSH, SCP for the SMX Vocational Training subject Network Services.
We use several computers to simulate a videowall in which we could display GUI applications.

./README.md		- This file
./configure.sh 		- Prepare the environment
./fantasticSSHMaster.sh - Delivers the client scripts to remote hosts and manages the execution
./fantasticSSHClient.sh - Remote scripts that will be called by fantasticSSHMaster.ssh

In the current exemple it simulates the Knight Rider led strip.
