#!/bin/bash

maquines[0]="192.168.28.112"
maquines[1]="192.168.28.32"
maquines[2]="192.168.28.31"
maquines[3]="192.168.28.163"
maquines[4]="192.168.28.22"
maquines[5]="192.168.28.96"


user='test'
pathPrivateKey='.ssh/id_rsa'

client="fantasticSSHClient.sh"

#Inicialització
echo "Copiant arxius"
for i in "${maquines[@]}"
do
    echo $i
    scp -i $pathPrivateKey $client $user@$i:/tmp 
    ssh -i $pathPrivateKey $user@$i chmod +x /tmp/$client
    ssh -i $pathPrivateKey $user@$i killall firefox 2>/dev/null
done


# Execució
echo "Executant en remot"


for repeat in 1 2 3 4 5 6 
do

for activa in 0 1 2 3 4 5 4 3 2 1
do

nMaq=0
for i in "${maquines[@]}"
do
	onoff='off'
	echo "Maquina activa $activa"

	if [ $nMaq -eq $activa ]
	then
		onoff='on'
	fi

	ssh -i $pathPrivateKey $user@$i /tmp/$client $onoff &
	echo "ssh -i $pathPrivateKey $user@$i /tmp/$client $onoff &"
	nMaq=$((nMaq+1))
done

	sleep 1
done
done