#!/bin/bash



maquines[0]="192.168.28.112"
maquines[1]="192.168.28.32"
maquines[2]="192.168.28.31"
maquines[3]="192.168.28.163"
maquines[4]="192.168.28.22"
maquines[5]="192.168.28.96"

pass='test2021'
user='test'

echo "Instal·lant programari necessari"
sudo apt -q install sshpass

echo "Generació de claus"
sshKeysPath='.ssh'
if [ ! -d "$sshKeysPath" ]
then
	echo " Creant directori de claus " $sshKeysPath
	mkdir -p $sshKeysPath
	echo " Generant claus"
	ssh-keygen -f $sshKeysPath/id_rsa -N ""
else
	echo " Les claus ja existeixen"
fi


echo "Copia de la clau en els equips remots"
for i in "${maquines[@]}"
do
	echo " Copiant a" $i
	#echo sshpass -p $pass ssh-copy-id -i $sshKeysPath/id_rsa.pub $user@$i
	sshpass -p $pass ssh-copy-id -i $sshKeysPath/id_rsa.pub $user@$i
done


